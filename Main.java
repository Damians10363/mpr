import repositories.ClientRepository;
import service.MockUnitOfWork;
import database.Client;
public class Main {
	private final String NAME = "MotorCompany";
	private final String ADDRESS = "AnvilStrasse";
	private final String CITY = "Bremen";
	private final int PHONENUMBER = 6095321;
	private final String NAME_2 = "MotorradWerkstatt";
	private final String ADDRESS_2 = "Grunwaldzka";
	private final String CITY_2 = "Sopot";
	private final int PHONENUMBER_2 = 6095321;
	
	private final Client CLIENT_1 = new Client(NAME, ADDRESS, CITY, PHONENUMBER, null);
	
	private ClientRepository cr;
	private MockUnitOfWork<Client> uof;
	
	private Client client;
	private Client client2;
	
	public void setUp() throws Exception {
		uof = new MockUnitOfWork<Client>();
		cr = new ClientRepository(uof);
	}
	cr.persistAdd(CLIENT_1);
	client = cr.get(id);
	client.setName(NAME_2);
	client.setAddress(ADDRESS_2);
	client.setCity(CITY_2);
	cr.persistUpdate(client);
	client2 = cr.get(id);