package bins;
import java.io.Serializable;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.model.ListDataModel;
import javax.inject.Inject;
import javax.inject.Named;

import database.Repair;
import repositories.RepairRepository;
import service.MockUnitOfWork;

@ApplicationScoped
@Named
public class RepairBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Repair repair;

	private ListDataModel<Repair> repairs = new ListDataModel<Repair>();

	@Inject
	private MockUnitOfWork< Repair> mockUnitOfWork;

	private RepairRepository rm = new RepairRepository(mockUnitOfWork);

	public Repair getRepair() {
		return repair;
	}

	public void setRepair(Repair repair) {
		this.repair = repair;
	}

	public ListDataModel<Repair> getAllRepairs() {
		repairs.setWrappedData(rm.getAll());
		return repairs;
	}

	// Actions
	public String addRepair() {
		rm.persistAdd(repair);
		return "showRepairs";
		//return null;
	}

	public String deleteRepair() {
		Repair personToDelete = repairs.getRowData();
		rm.persistDelete(personToDelete);
		return null;
	}}