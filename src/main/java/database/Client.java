package database;

import java.util.ArrayList;

public class Client extends EntityBase{
	
	private String name;
	private String address;
	private String city;
	private int phonenumber;
	private ArrayList<Repair> repairs;
	
	public Client(String name, String address, String city, int phonenumber,
			ArrayList<Repair> repairs) {
		super();
		this.name = name;
		this.address = address;
		this.city = city;
		this.phonenumber = phonenumber;
		this.repairs = repairs;
	}
	public Client() {
		// TODO Auto-generated constructor stub
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public int getPhonenumber() {
		return phonenumber;
	}
	public void setPhonenumber(int phonenumber) {
		this.phonenumber = phonenumber;
	}
	public ArrayList<Repair> getRepairs() {
		return repairs;
	}
	public void setRepairs(ArrayList<Repair> repairs) {
		this.repairs = repairs;
	}
	
}
