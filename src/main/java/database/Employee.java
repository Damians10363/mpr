package database;

import java.util.List;

public class Employee extends EntityBase{

private String name;
private String surname;
private int dob;
private int salary;
private int phonenumber;



public Employee(String name, String surname, int dob,
		int salary, int phonenumber, List<Repair> repairs) {
	super();
	this.name = name;
	this.surname = surname;
	this.dob = dob;
	this.salary = salary;
	this.phonenumber = phonenumber;
}

public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getSurname() {
	return surname;
}
public void setSurname(String surname) {
	this.surname = surname;
}
public int getDob() {
	return dob;
}
public void setDob(int dob) {
	this.dob = dob;
}
public int getSalary() {
	return salary;
}
public void setSalary(int salary) {
	this.salary = salary;
}
public int getPhonenumber() {
	return phonenumber;
}
public void setPhonenumber(int phonenumber) {
	this.phonenumber = phonenumber;
}

public Employee() {
	super();
	// TODO Auto-generated constructor stub
}
}
