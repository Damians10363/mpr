package database;

public class EmployeeToRepair {
	
	private int employeeId;
	private int repairId;
	
	public EmployeeToRepair(){}

	public EmployeeToRepair(int employeeId, int repairId) {
		super();
		this.employeeId = employeeId;
		this.repairId = repairId;
	}

	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	public int getRepairId() {
		return repairId;
	}

	public void setRepairId(int repairId) {
		this.repairId = repairId;
	}
	
	

}
