package database;

public class EntityBase {
	
	private int id;


	public EntityBase() {
		super();
	}

	public EntityBase(int id) {
		super();
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	

}
