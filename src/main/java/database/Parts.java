package database;

import java.util.List;

public class Parts extends EntityBase{

private String name;
private String producer;
private int cost;
private List<Repair> repairs;

public Parts(String name, String producer, int cost,
		List<Repair> repairs) {
	super();
	this.name = name;
	this.producer = producer;
	this.cost = cost;
	this.repairs = repairs;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getProducer() {
	return producer;
}
public void setProducer(String producer) {
	this.producer = producer;
}
public int getCost() {
	return cost;
}
public void setCost(int cost) {
	this.cost = cost;
}
public List<Repair> getRepairs() {
	return repairs;
}
public void setRepairs(List<Repair> repairs) {
	this.repairs = repairs;
}

}
