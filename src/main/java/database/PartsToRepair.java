package database;

public class PartsToRepair {
	private int partsId;
	private int repairId;
	
	private PartsToRepair(){}
	
	


	public PartsToRepair(int partsId, int repairId) {
		super();
		this.partsId = partsId;
		this.repairId = repairId;
	}




	public int getPartsId() {
		return partsId;
	}


	public void setPartsId(int partsId) {
		this.partsId = partsId;
	}


	public int getRepairId() {
		return repairId;
	}


	public void setRepairId(int repairId) {
		this.repairId = repairId;
	}
	
	
}
