 package database;


public class Repair extends EntityBase{
		
	private int ClientId; 
	private int MotobikeId; 
	private int WorkplaceId;
	
	public Repair(int clientId, int motobikeId, int workplaceId) {
		super();
		ClientId = clientId;
		MotobikeId = motobikeId;
		WorkplaceId = workplaceId;
	}
	public Repair() {
		// TODO Auto-generated constructor stub
	}
	public int getClientId() {
		return ClientId;
	}
	public void setClientId(int clientId) {
		ClientId = clientId;
	}
	public int getMotobikeId() {
		return MotobikeId;
	}
	public void setMotobikeId(int motobikeId) {
		MotobikeId = motobikeId;
	}
	public int getWorkplaceId() {
		return WorkplaceId;
	}
	public void setWorkplaceId(int workplaceId) {
		WorkplaceId = workplaceId;
	}
}
	
	
