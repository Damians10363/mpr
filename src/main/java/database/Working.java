package database;

import java.util.List;

public class Working extends EntityBase{

	private String name;
	private int cost;
	

	public Working(String name, int cost, List<Repair> repairs) {
		super();
		this.name = name;
		this.cost = cost;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getCost() {
		return cost;
	}
	public void setCost(int cost) {
		this.cost = cost;
	}
}
