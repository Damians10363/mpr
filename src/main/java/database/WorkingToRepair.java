package database;

public class WorkingToRepair {
	
	private int workingId;
	private int repairId;
	
	public WorkingToRepair() {}

	public WorkingToRepair(int workingId, int repairId) {
		super();
		this.workingId = workingId;
		this.repairId = repairId;
	}

	public int getWorkingId() {
		return workingId;
	}

	public void setWorkingId(int workingId) {
		this.workingId = workingId;
	}

	public int getRepairId() {
		return repairId;
	}

	public void setRepairId(int repairId) {
		this.repairId = repairId;
	}
	
	

}
