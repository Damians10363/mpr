package dto;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

import database.Repair;

@XmlRootElement
public class ClientDto extends ClientSummaryDto {
private ArrayList<Repair> repairs;

	public ArrayList<Repair> getRepairs() {
		return (ArrayList<Repair>) repairs;
	}

	public void setRepairs(ArrayList<Repair> indents) {
		this.repairs = repairs;
	}



}