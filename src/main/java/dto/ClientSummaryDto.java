package dto;

public class ClientSummaryDto {

	private String name;
	private String address;
	private String city;
	private int phonenumber;
	
	public ClientSummaryDto(String name, String address, String city,
			int phonenumber) {
		super();
		this.name = name;
		this.address = address;
		this.city = city;
		this.phonenumber = phonenumber;
	}
	public ClientSummaryDto() {
		// TODO Auto-generated constructor stub
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public int getPhonenumber() {
		return phonenumber;
	}
	public void setPhonenumber(int phonenumber) {
		this.phonenumber = phonenumber;
	}

}
