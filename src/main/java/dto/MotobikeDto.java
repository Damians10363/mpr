package dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class MotobikeDto extends MotobikeSummaryDto {

	//public MotobikeDto(){}
		
	
	public MotobikeDto(String licenseplate, String brand, String type, int enginecapacity,
			int timesunderrepair) {
		super(licenseplate, brand, type, enginecapacity, timesunderrepair);
		// TODO Auto-generated constructor stub
	}

	public MotobikeDto() {
		// TODO Auto-generated constructor stub
	}
	

}