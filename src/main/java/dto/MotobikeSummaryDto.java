package dto;

public class MotobikeSummaryDto {

	private String licenseplate;
	private String brand;
	private String type;
	private int enginecapacity;
	private int timesunderrepair;
	
		public MotobikeSummaryDto(String licenseplate, String brand, String type,
			int enginecapacity, int timesunderrepair) {
		super();
		this.licenseplate = licenseplate;
		this.brand = brand;
		this.type = type;
		this.enginecapacity = enginecapacity;
		this.timesunderrepair = timesunderrepair;
	}
	public MotobikeSummaryDto() {
			// TODO Auto-generated constructor stub
		}
	public String getLicenseplate() {
		return licenseplate;
	}
	public void setLicenseplate(String licenseplate) {
		this.licenseplate = licenseplate;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getEnginecapacity() {
		return enginecapacity;
	}
	public void setEnginecapacity(int enginecapacity) {
		this.enginecapacity = enginecapacity;
	}
	public int getTimesunderrepair() {
		return timesunderrepair;
	}
	public void setTimesunderrepair(int timesunderrepair) {
		this.timesunderrepair = timesunderrepair;
	}	
	
	
}


