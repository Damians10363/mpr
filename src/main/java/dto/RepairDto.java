package dto;

import javax.xml.bind.annotation.XmlRootElement;

public class RepairDto extends RepairSummaryDto{

	private int ClientId;
	private int MotobikeId;
	private int WorkplaceId;
	public int getClientId() {
		return ClientId;
	}
	public void setClientId(int clientId) {
		ClientId = clientId;
	}
	public int getMotobikeId() {
		return MotobikeId;
	}
	public void setMotobikeId(int motobikeId) {
		MotobikeId = motobikeId;
	}
	public int getWorkplaceId() {
		return WorkplaceId;
	}
	public void setWorkplaceId(int workplaceId) {
		WorkplaceId = workplaceId;
	}
	
}
