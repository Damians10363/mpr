package mockrepositories;

import database.Repair;
import service.DBContext;
import service.MockDb;
import service.MockUnitOfWork;

public class RepairRepository extends DBContext<Repair>{

	public RepairRepository(MockUnitOfWork<Repair> unitofwork, MockDb<Repair> db) {
		super(unitofwork, db);
	}

}
