package repositories;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import database.Client;
import service.MockUnitOfWork;
import service.SQLContext;

public class ClientRepository extends SQLContext<Client>{
	
	private PreparedStatement getByName;
	private PreparedStatement getByAddress;
	private PreparedStatement getByCity;
	private PreparedStatement getByPhonenumber;
	
	private RepairRepository or;
	
	public ClientRepository(MockUnitOfWork<Client> unitofwork) {
		super(unitofwork);
		try {stmt = connection.createStatement();
		ResultSet rs = connection.getMetaData().getTables(null, null, null, null);
		boolean exist = false;
		while (rs.next()){
			if ("client".equalsIgnoreCase(rs.getString("TABLE_NAME"))){
				exist = true;
				logger.info("table exist");
				break;
			}
		}
		
		if(!exist){
			logger.info("table not exist, creating table . . .");
			stmt.executeUpdate("CREATE TABLE `client` ( `id` smallint(6) NOT NULL AUTO_INCREMENT, `name` varchar(30) NOT NULL, `address` varchar(45) DEFAULT NULL, `city` varchar(30) DEFAULT NULL, `phonenumber` int(11) DEFAULT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
			logger.info("table created");
		}
		
		getById = connection
				.prepareStatement("SELECT * FROM client WHERE id =?");
		getByName = connection
				.prepareStatement("SELECT * FROM client WHERE name=?");
		getByAddress = connection
				.prepareStatement("SELECT * FROM client WHERE address=?");
		getByCity = connection
				.prepareStatement("SELECT * FROM client WHERE city=?");
		getByPhonenumber = connection
				.prepareStatement("SELECT * FROM client WHERE phonenumber =?");
		insert = connection
				.prepareStatement("INSERT INTO client (name, address, city, phonenumber) VALUES (?,?,?,?)");
		delete = connection
				.prepareStatement("DELETE FROM client WHERE name=? and address=?");
		update = connection
				.prepareStatement("UPDATE client SET name=?, address=?, city=? phonenumber=? WHERE id=?");
		getAll = connection.prepareStatement("SELECT * FROM client");
		deleteAll = connection.prepareStatement("DELETE FROM client");
	} catch (SQLException e) {
		logger.fatal(e);
		}
	}

	@Override
	public void persistAdd(Client obj) {
		try {
			insert.setString(1, obj.getName());
			insert.setString(2, obj.getAddress());
			insert.setString(3, obj.getCity());
			insert.setInt(4, obj.getPhonenumber());
			insert.executeUpdate();
		} catch (SQLException e) {
			logger.error(e);
		}
	}

	@Override
	public void persistUpdate(Client obj) {
		try {
			update.setString(1, obj.getName());
			update.setString(2, obj.getAddress());
			update.setString(3, obj.getCity());
			insert.setInt(4, obj.getPhonenumber());
			update.setInt(5, obj.getId());
			update.executeUpdate();
		} catch (SQLException e) {
			logger.error(e);
		}
	}

	@Override
	public void persistDelete(Client obj) {
		try {
			delete.setString(1, obj.getName());
			delete.setString(2, obj.getAddress());
			delete.executeUpdate();
		} catch (SQLException e) {
			logger.error(e);
		}
	}

	@Override
	public Client get(int id) {
		Client client = new Client();
		try {
			getById.setInt(1, id);
			ResultSet rs = getById.executeQuery();
			while (rs.next()) {
				client.setId(rs.getInt("id"));
				client.setName(rs.getString("name"));
				client.setAddress(rs.getString("address"));
				client.setCity(rs.getString("city"));
				client.setPhonenumber(rs.getInt("phonenumber"));
				client.setRepairs(or.getByClientId(id));
			}
		} catch (SQLException e) {
			logger.error(e);
		}
		return client;
	}

	public Client getByName(String name) {
		Client client = new Client();
		try {
			getByName.setString(1, name);
			ResultSet rs = getByName.executeQuery();
			while (rs.next()) {
				client.setId(rs.getInt("id"));
				client.setName(rs.getString("name"));
				client.setAddress(rs.getString("address"));
				client.setCity(rs.getString("city"));
				client.setPhonenumber(rs.getInt("phonenumber"));
				client.setRepairs(or.getByClientId(client.getId()));
			}
		} catch (SQLException e) {
			logger.error(e);
		}
		return client;
	}

	public Client getByCity(String city) {
		Client client = new Client();
		try {
			getByCity.setString(1, city);
			ResultSet rs = getByCity.executeQuery();
			while (rs.next()) {
				client.setId(rs.getInt("id"));
				client.setName(rs.getString("name"));
				client.setAddress(rs.getString("address"));
				client.setCity(rs.getString("city"));
				client.setPhonenumber(rs.getInt("phonenumber"));
				client.setRepairs(or.getByClientId(client.getId()));
			}
		} catch (SQLException e) {
			logger.error(e);
		}
		return client;
	}

	public Client getByAddress(String address) {
		Client client = new Client();
		try {
			getByAddress.setString(1, address);
			ResultSet rs = getByAddress.executeQuery();
			while (rs.next()) {
				client.setId(rs.getInt("id"));
				client.setName(rs.getString("name"));
				client.setAddress(rs.getString("address"));
				client.setCity(rs.getString("city"));
				client.setPhonenumber(rs.getInt("phonenumber"));
				client.setRepairs(or.getByClientId(client.getId()));
			}
		} catch (SQLException e) {
			logger.error(e);
		}
		return client;
	}
	
	public Client getByAPhonenumber(int phonenumber) {
		Client client = new Client();
		try {
			getByPhonenumber.setInt(1, phonenumber);
			ResultSet rs = getByPhonenumber.executeQuery();
			while (rs.next()) {
				client.setId(rs.getInt("id"));
				client.setName(rs.getString("name"));
				client.setAddress(rs.getString("address"));
				client.setCity(rs.getString("city"));
				client.setPhonenumber(rs.getInt("phonenumber"));
				client.setRepairs(or.getByClientId(client.getId()));
			}
		} catch (SQLException e) {
			logger.error(e);
		}
		return client;
	}

	@Override
	public ArrayList<Client> getAll() {
		ArrayList<Client> result = new ArrayList<Client>();
		try {
			ResultSet rs = getAll.executeQuery();
			while (rs.next()) {
				Client client = new Client();
				client.setId(rs.getInt("id"));
				client.setName(rs.getString("name"));
				client.setAddress(rs.getString("address"));
				client.setCity(rs.getString("city"));
				client.setPhonenumber(rs.getInt("phonenumber"));
				client.setRepairs(or.getByClientId(client.getId()));
				result.add(client);
			}
		} catch (SQLException e) {
			logger.error(e);
		}
		return result;
	}

}
