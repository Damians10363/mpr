package repositories;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import database.Employee;
import service.MockUnitOfWork;
import service.SQLContext;

public class EmployeeRepository extends SQLContext<Employee> {

	private PreparedStatement getByName;
	private PreparedStatement getBySurname;
	private PreparedStatement getByDob;
	private PreparedStatement getBySalary;
	private PreparedStatement getByPhonenumber;



	public EmployeeRepository(MockUnitOfWork<Employee> unitofwork) {
		super(unitofwork);

		try {
			stmt = connection.createStatement();
			ResultSet rs = connection.getMetaData().getTables(null, null, null,
					null);
			boolean exist = false;
			while (rs.next()) {
				if ("employee".equalsIgnoreCase(rs.getString("TABLE_NAME"))) {
					exist = true;
					logger.info("table exist");
					break;
				}
			}
			if (!exist) {
				logger.info("table not exist, creating table...");
				stmt.executeUpdate("CREATE TABLE `employee` ( `id` smallint(6) NOT NULL AUTO_INCREMENT, `name` varchar(30) NOT NULL, `surname` varchar(30) NOT NULL, `dob` int(15) DEFAULT NULL, `salary` int(11) DEFAULT NULL, `phonenumber` int(15) DEFAULT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
				logger.info("table created");
			}

			getById = connection
					.prepareStatement("SELECT * FROM employee WHERE id =?");
			getByName = connection
					.prepareStatement("SELECT * FROM employee WHERE name=?");
			getBySurname = connection
					.prepareStatement("SELECT * FROM employee WHERE surname=?");
			getByDob = connection
					.prepareStatement("SELECT * FROM employee WHERE dob=?");
			getBySalary = connection
					.prepareStatement("SELECT * FROM employee WHERE salary=?");
			getByPhonenumber = connection
					.prepareStatement("SELECT * FROM employee WHERE phonenumber=?");
			insert = connection
					.prepareStatement("INSERT INTO employee (name, surname, dob, salary, phonenumber) VALUES (?,?,?,?,?)");
			delete = connection
					.prepareStatement("DELETE FROM employee WHERE id=?");
			update = connection
					.prepareStatement("UPDATE employee SET name=?, surname=?, dob=?, salary=?, phonenumber=? WHERE id=?");
			getAll = connection.prepareStatement("SELECT * FROM employee");
			deleteAll = connection.prepareStatement("DELETE FROM employee");
		} catch (SQLException e) {
			logger.fatal(e);
		}
	}

	@Override
	public void persistAdd(Employee obj) {
		try {
			insert.setString(1, obj.getName());
			insert.setString(2, obj.getSurname());
			insert.setInt(3, obj.getDob());
			insert.setInt(4, obj.getSalary());
			insert.setInt(5, obj.getPhonenumber());
			update.setInt(6, obj.getId());
			insert.executeUpdate();
		} catch (SQLException e) {
			logger.error(e);
		}
	}

	@Override
	public void persistUpdate(Employee obj) {
		try {
			update.setString(1, obj.getName());
			update.setString(2, obj.getSurname());
			update.setInt(3, obj.getDob());
			update.setInt(4, obj.getSalary());
			update.setInt(5, obj.getPhonenumber());
			update.executeUpdate();
		} catch (SQLException e) {
			logger.error(e);
		}
	}

	@Override
	public void persistDelete(Employee obj) {
		try {
			delete.setInt(1, obj.getId());
			delete.executeUpdate();
		} catch (SQLException e) {
			logger.error(e);
		}
	}

	@Override
	public Employee get(int id) {
		Employee employee = new Employee();
		try {
			getById.setInt(1, id);
			ResultSet rs = getById.executeQuery();
			while (rs.next()) {

				employee.setId(rs.getInt("id"));
				employee.setName(rs.getString("name"));	
				employee.setSurname(rs.getString("surname"));
				employee.setDob(rs.getInt("dob"));
				employee.setSalary(rs.getInt("salary"));
				employee.setPhonenumber(rs.getInt("phonenumber"));

			}
		} catch (SQLException e) {
			logger.error(e);
		}

		return employee;
	}

	public ArrayList<Employee> getByName(String name) {
		ArrayList<Employee> result = new ArrayList<Employee>();
		try {
			getByName.setString(1, name);
			ResultSet rs = getByName.executeQuery();
			while (rs.next()) {
				Employee employee = new Employee();
				employee.setId(rs.getInt("id"));
				employee.setName(rs.getString("name"));
				employee.setSurname(rs.getString("surname"));
				employee.setDob(rs.getInt("dob"));
				employee.setSalary(rs.getInt("salary"));
				employee.setPhonenumber(rs.getInt("phonenumber"));
				result.add(employee);

			}
		} catch (SQLException e) {
			logger.error(e);
		}

		return result;
	}

	public ArrayList<Employee> getBySurname(String surname) {
		ArrayList<Employee> result = new ArrayList<Employee>();
		try {
			getBySurname.setString(1, surname);
			ResultSet rs = getBySurname.executeQuery();
			while (rs.next()) {
				Employee employee = new Employee();
				employee.setId(rs.getInt("id"));
				employee.setName(rs.getString("name"));
				employee.setSurname(rs.getString("surname"));
				employee.setDob(rs.getInt("dob"));
				employee.setSalary(rs.getInt("salary"));
				employee.setPhonenumber(rs.getInt("phonenumber"));
				result.add(employee);

			}
		} catch (SQLException e) {
			logger.error(e);
		}

		return result;
	}

	public ArrayList<Employee> getByDob(int dob) {
		ArrayList<Employee> result = new ArrayList<Employee>();
		try {
			getByDob.setInt(1, dob);
			ResultSet rs = getByDob.executeQuery();
			while (rs.next()) {
				Employee employee = new Employee();
				employee.setId(rs.getInt("id"));
				employee.setName(rs.getString("name"));
				employee.setSurname(rs.getString("surname"));
				employee.setDob(rs.getInt("dob"));
				employee.setSalary(rs.getInt("salary"));
				employee.setPhonenumber(rs.getInt("phonenumber"));
				result.add(employee);

			}
		} catch (SQLException e) {
			logger.error(e);
		}

		return result;
	}

	
	public ArrayList<Employee> getBySalary(int salary) {
		ArrayList<Employee> result = new ArrayList<Employee>();
		try {
			getBySalary.setInt(1, salary);
			ResultSet rs = getBySalary.executeQuery();
			while (rs.next()) {
				Employee employee = new Employee();
				employee.setId(rs.getInt("id"));
				employee.setName(rs.getString("name"));
				employee.setSurname(rs.getString("surname"));
				employee.setDob(rs.getInt("dob"));
				employee.setSalary(rs.getInt("salary"));
				employee.setPhonenumber(rs.getInt("phonenumber"));
				result.add(employee);

			}
		} catch (SQLException e) {
			logger.error(e);
		}

		return result;
	}
	
	public ArrayList<Employee> getByPhonenumber(int phonenumber) {
		ArrayList<Employee> result = new ArrayList<Employee>();
		try {
			getByPhonenumber.setInt(1, phonenumber);
			ResultSet rs = getByPhonenumber.executeQuery();
			while (rs.next()) {
				Employee employee = new Employee();
				employee.setId(rs.getInt("id"));
				employee.setName(rs.getString("name"));
				employee.setSurname(rs.getString("surname"));
				employee.setDob(rs.getInt("dob"));
				employee.setSalary(rs.getInt("salary"));
				employee.setPhonenumber(rs.getInt("phonenumber"));
				result.add(employee);
			}
		} catch (SQLException e) {
			logger.error(e);
		}

		return result;
	}
	@Override
	public ArrayList<Employee> getAll() {
		ArrayList<Employee> result = new ArrayList<Employee>();
		try {
			ResultSet rs = getAll.executeQuery();
			while (rs.next()) {
				Employee employee = new Employee();
				employee.setId(rs.getInt("id"));
				employee.setName(rs.getString("name"));
				employee.setSurname(rs.getString("surname"));
				employee.setDob(rs.getInt("dob"));
				employee.setSalary(rs.getInt("salary"));
				employee.setPhonenumber(rs.getInt("phonenumber"));
				result.add(employee);
			}
		} catch (SQLException e) {
			logger.error(e);
		}

		return result;
	}

}