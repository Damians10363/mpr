package repositories;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import database.Motobike;
import service.MockUnitOfWork;
import service.SQLContext;

public class MotobikeRepository extends SQLContext<Motobike>{

	private PreparedStatement getByBrand;
	private PreparedStatement getByLicenseplate;
	private PreparedStatement getByType;
	private PreparedStatement getByEnginecapacity;
	private PreparedStatement getByTimesunderrepair;
	

	public MotobikeRepository(MockUnitOfWork<Motobike> unitofwork) {
		super(unitofwork);
		try {
			stmt = connection.createStatement();
			ResultSet rs = connection.getMetaData().getTables(null, null, null,
					null);
			boolean exist = false;
			while (rs.next()) {
				if ("motobike".equalsIgnoreCase(rs.getString("TABLE_NAME"))) {
					exist = true;
					logger.info("table exist");
					break;
				}
			}
			if (!exist) {
				logger.info("table not exist, creating table...");
				stmt.executeUpdate("CREATE TABLE `motobike` (`id` smallint(6) NOT NULL AUTO_INCREMENT, `licenseplate` varchar(15) NOT NULL, `brand` varchar(30) DEFAULT NULL, `type` varchar(30) DEFAULT NULL, `enginecapacity` int(11) DEFAULT NULL, `timesunderrepair` int(4) DEFAULT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
				logger.info("table created");
			}

			getById = connection
					.prepareStatement("SELECT * FROM motobike WHERE id=?");
			getByBrand = connection
					.prepareStatement("SELECT * FROM motobike WHERE brand=?");
			getByType = connection
					.prepareStatement("SELECT * FROM motobike WHERE type=?");
			getByEnginecapacity = connection
					.prepareStatement("SELECT * FROM motobike WHERE enginecapacity=?");
			getByLicenseplate = connection
					.prepareStatement("SELECT * FROM motobike WHERE licenseplate=?");
			getByTimesunderrepair = connection
					.prepareStatement("SELECT * FROM motobike WHERE timesunderrepair=?");
			insert = connection
					.prepareStatement("INSERT INTO motobike (brand, type, enginecapacity, licenseplate, timesunderrepair) VALUES (?,?,?,?,?,?)");
			delete = connection
					.prepareStatement("DELETE FROM motobike WHERE licenseplate=?");
			update = connection
					.prepareStatement("UPDATE motobike SET brand=?, type=?, enginecapacity=?, licenseplate=?, timesunderrepair=? WHERE id=?");
			getAll = connection.prepareStatement("SELECT * FROM motobike");
			deleteAll = connection.prepareStatement("DELETE FROM motobike");
		} catch (SQLException e) {
			logger.error(e);
		}

	}

	public void persistAdd(Motobike obj) {
		try {
			insert.setString(1, obj.getBrand());
			insert.setString(2, obj.getType());
			insert.setInt(3, obj.getEnginecapacity());
			insert.setString(4, obj.getLicenseplate());
			insert.setInt(5, obj.getTimesunderrepair());
			insert.executeUpdate();

		} catch (SQLException e) {
			logger.fatal(e);
		}
	}

	public void persistUpdate(Motobike obj) {
		try {
			update.setString(1, obj.getBrand());
			update.setString(2, obj.getType());
			update.setInt(3, obj.getEnginecapacity());
			update.setString(4, obj.getLicenseplate());
			update.setInt(5, obj.getTimesunderrepair());
			update.setInt(6, obj.getId());
			update.executeUpdate();
		} catch (SQLException e) {
			logger.error(e);
		}

	}

	public void persistDelete(Motobike obj) {
		try {
			delete.setString(1, obj.getLicenseplate());
			delete.executeUpdate();
		} catch (SQLException e) {
			logger.error(e);
		}
	}

	public Motobike get(int id) {

		Motobike motobike = new Motobike();
		try {
			getById.setInt(1, id);
			ResultSet rs = getById.executeQuery();
			while (rs.next()) {
				motobike.setId(rs.getInt("id"));
				motobike.setBrand(rs.getString("brand"));
				motobike.setType(rs.getString("type"));
				motobike.setEnginecapacity(rs.getInt("enginecapacity"));
				motobike.setLicenseplate(rs.getString("licenseplate"));
				motobike.setTimesunderrepair(rs.getInt("timesunderrepair"));
			}
		} catch (SQLException e) {
			logger.error(e);
		}
		return motobike;
	}

	public Motobike getByLicensePlate(String licensePlate) {
		Motobike motobike = new Motobike();
		try {
			getByLicenseplate.setString(1, licensePlate);
			ResultSet rs = getByLicenseplate.executeQuery();
			while (rs.next()) {
				motobike.setId(rs.getInt("id"));
				motobike.setBrand(rs.getString("brand"));
				motobike.setType(rs.getString("type"));
				motobike.setEnginecapacity(rs.getInt("enginecapacity"));
				motobike.setLicenseplate(rs.getString("licenseplate"));
				motobike.setTimesunderrepair(rs.getInt("timesunderrepair"));
			}
		} catch (SQLException e) {
			logger.error(e);
		}
		return motobike;
	}
	
	public Motobike getByEnginecapacity(int enginecapacity) {
		Motobike motobike = new Motobike();
		try {
			getByEnginecapacity.setInt(1, enginecapacity);
			ResultSet rs = getByEnginecapacity.executeQuery();
			while (rs.next()) {
				motobike.setId(rs.getInt("id"));
				motobike.setBrand(rs.getString("brand"));
				motobike.setType(rs.getString("type"));
				motobike.setEnginecapacity(rs.getInt("enginecapacity"));
				motobike.setLicenseplate(rs.getString("licenseplate"));
				motobike.setTimesunderrepair(rs.getInt("timesunderrepair"));
			}
		} catch (SQLException e) {
			logger.error(e);
		}
		return motobike;
	}

	
	public Motobike getByTimesunderrepair(int timesunderrepair) {
		Motobike motobike = new Motobike();
		try {
			getByTimesunderrepair.setInt(1, timesunderrepair);
			ResultSet rs = getByTimesunderrepair.executeQuery();
			while (rs.next()) {
				motobike.setId(rs.getInt("id"));
				motobike.setBrand(rs.getString("brand"));
				motobike.setType(rs.getString("type"));
				motobike.setEnginecapacity(rs.getInt("enginecapacity"));
				motobike.setLicenseplate(rs.getString("licenseplate"));
				motobike.setTimesunderrepair(rs.getInt("timesunderrepair"));
			}
		} catch (SQLException e) {
			logger.error(e);
		}
		return motobike;
	}
	public ArrayList<Motobike> getByBrand(String brand) {
		ArrayList<Motobike> result = new ArrayList<Motobike>();
		try {
			getByBrand.setString(1, brand);
			ResultSet rs = getByBrand.executeQuery();
			while (rs.next()) {
				Motobike motobike = new Motobike();
				motobike.setId(rs.getInt("id"));
				motobike.setBrand(rs.getString("brand"));
				motobike.setType(rs.getString("Type"));
				motobike.setEnginecapacity(rs.getInt("enginecapacity"));
				motobike.setLicenseplate(rs.getString("licenseplate"));
				motobike.setTimesunderrepair(rs.getInt("timesunderrepair"));
				result.add(motobike);
			}
		} catch (SQLException e) {
			logger.error(e);
		}
		return result;
	}

	public ArrayList<Motobike> getByType(String type) {
		ArrayList<Motobike> result = new ArrayList<Motobike>();
		Motobike motobike = new Motobike();
		try {
			getByType.setString(1, type);
			ResultSet rs = getByType.executeQuery();
			while (rs.next()) {
				motobike.setId(rs.getInt("id"));
				motobike.setBrand(rs.getString("brand"));
				motobike.setType(rs.getString("type"));
				motobike.setEnginecapacity(rs.getInt("enginecapacity"));
				motobike.setLicenseplate(rs.getString("licenseplate"));
				motobike.setTimesunderrepair(rs.getInt("timesunderrepair"));
				result.add(motobike);
			}
		} catch (SQLException e) {
			logger.error(e);
		}
		return result;
	}

	public ArrayList<Motobike> getAll() {
		ArrayList<Motobike> result = new ArrayList<Motobike>();
		try {
			ResultSet rs = getAll.executeQuery();
			while (rs.next()) {
				Motobike motobike = new Motobike();
				motobike.setId(rs.getInt("id"));
				motobike.setBrand(rs.getString("brand"));
				motobike.setType(rs.getString("type"));
				motobike.setEnginecapacity(rs.getInt("enginecapacity"));
				motobike.setLicenseplate(rs.getString("licenseplate"));
				motobike.setTimesunderrepair(rs.getInt("timesunderrepair"));
				result.add(motobike);
			}
		} catch (SQLException e) {
			logger.error(e);
		}
		return result;
	}

}
