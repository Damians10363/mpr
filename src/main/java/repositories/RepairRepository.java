package repositories;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import database.Repair;
import service.MockUnitOfWork;
import service.SQLContext;

public class RepairRepository extends SQLContext<Repair>{
	
	private PreparedStatement getByClientId;
	private PreparedStatement getByMotobikeId;
	private PreparedStatement getByWorkplaceId;

	public RepairRepository(MockUnitOfWork<Repair> unitofwork){
		super(unitofwork);
		try {stmt = connection.createStatement();
		ResultSet rs = connection.getMetaData().getTables(null, null, null, null);
		boolean exist = false;
		while (rs.next()){
			if ("repair".equalsIgnoreCase(rs.getString("TABLE_NAME"))){
				exist = true;
				logger.info("table exist");
				break;
			}
		}
		
		if(!exist){
			logger.info("table not exist, creating table . . .");
			stmt.executeUpdate("CREATE TABLE `client` ( `id` smallint(6) NOT NULL AUTO_INCREMENT, `ClientId` smallint(6) NOT NULL, `MotobikeId` smallint(6) NOT NULL, `WorkplaceId` smallint(6) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
			logger.info("table created");
		}
	
		getById = connection
				.prepareStatement("SELECT * FROM repair WHERE id =?");
		getByClientId = connection
				.prepareStatement("SELECT * FROM repair WHERE ClientId=?");
		getByMotobikeId = connection
				.prepareStatement("SELECT * FROM repair WHERE MotobikeId=?");
		getByWorkplaceId = connection
				.prepareStatement("SELECT * FROM repair WHERE WorkplaceId=?");
		update = connection
				.prepareStatement("UPDATE indent SET ClientId=?, MotobikeId=?, WorkplaceId=? where id=?");
		insert = connection
				.prepareStatement("INSERT INTO repair (ClientId, MotobikeId, WorkplaceId) VALUES (?,?)");
		delete = connection
				.prepareStatement("DELETE FROM repair WHERE id=?");
		getAll = connection.prepareStatement("SELECT * FROM repair");
		deleteAll = connection.prepareStatement("DELETE FROM repair");
	} catch (SQLException e) {
		e.printStackTrace();
	}
}
	@Override
	public void persistAdd(Repair obj) {
		try {
			insert.setInt(1, obj.getClientId());
			insert.setInt(2, obj.getMotobikeId());
			insert.setInt(3, obj.getWorkplaceId());
			insert.executeUpdate();
		} catch (SQLException e) {
			logger.fatal(e);
		}
	}

	@Override
	public void persistUpdate(Repair obj) {
		try {
			update.setInt(1, obj.getClientId());
			update.setInt(2, obj.getMotobikeId());
			insert.setInt(3, obj.getWorkplaceId());
			update.setInt(4, obj.getId());
			update.executeUpdate();
		} catch (SQLException e) {
			logger.error(e);
		}

	}

	@Override
	public void persistDelete(Repair obj) {
		try {
			delete.setInt(1, obj.getId());
			delete.executeUpdate();
		} catch (SQLException e) {
			logger.error(e);
		}

	}

	@Override
	public Repair get(int id) {
		Repair repair = new Repair();
		try {
			getById.setInt(1, id);
			ResultSet rs = getById.executeQuery();
			while (rs.next()) {
				repair.setId(rs.getInt("id"));
				repair.setClientId(rs.getInt("ClientId"));
				repair.setMotobikeId(rs.getInt("MotobikeId"));
				repair.setWorkplaceId(rs.getInt("WorkplaceId"));
			}
		} catch (SQLException e) {
			logger.error(e);
		}
		return repair;
	}

	public ArrayList<Repair> getByClientId(int id) {
		ArrayList<Repair> result = new ArrayList<Repair>();
		try {
			getByClientId.setInt(1, id);
			ResultSet rs = getByClientId.executeQuery();
			while (rs.next()) {
				Repair repair = new Repair();
				repair.setId(rs.getInt("id"));
				repair.setClientId(rs.getInt("ClientId"));
				repair.setMotobikeId(rs.getInt("MotobikeId"));
				repair.setWorkplaceId(rs.getInt("WorkplaceId"));
				result.add(repair);
			}
		} catch (SQLException e) {
			logger.error(e);
		}
		return result;
	}

	public ArrayList<Repair> getByMotobikeId(int id) {
		ArrayList<Repair> result = new ArrayList<Repair>();
		try {
			getByMotobikeId.setInt(1, id);
			ResultSet rs = getByMotobikeId.executeQuery();
			while (rs.next()) {
				Repair repair = new Repair();
				repair.setId(rs.getInt("id"));
				repair.setClientId(rs.getInt("ClientId"));
				repair.setMotobikeId(rs.getInt("MotobikeId"));
				repair.setWorkplaceId(rs.getInt("WorkplaceId"));
				result.add(repair);

			}
		} catch (SQLException e) {
			logger.error(e);
		}
		return result;
	}

	public ArrayList<Repair> getByWorkingplaceId(int id) {
		ArrayList<Repair> result = new ArrayList<Repair>();
		try {
			getByWorkplaceId.setInt(1, id);
			ResultSet rs = getByWorkplaceId.executeQuery();
			while (rs.next()) {
				Repair repair = new Repair();
				repair.setId(rs.getInt("id"));
				repair.setClientId(rs.getInt("ClientId"));
				repair.setMotobikeId(rs.getInt("MotobikeId"));
				repair.setWorkplaceId(rs.getInt("WorkplaceId"));
				result.add(repair);
			}
		} catch (SQLException e) {
			logger.error(e);
		}
		return result;
	}
	
	@Override
	public ArrayList<Repair> getAll() {
		ArrayList<Repair> result = new ArrayList<Repair>();
		try {
			ResultSet rs = getAll.executeQuery();
			while (rs.next()) {
				Repair repair = new Repair();
				repair.setId(rs.getInt("id"));
				repair.setClientId(rs.getInt("courierId"));
				repair.setMotobikeId(rs.getInt("MotobikeId"));
				repair.setWorkplaceId(rs.getInt("WorkplaceId"));
				result.add(repair);
			}
		} catch (SQLException e) {
			logger.error(e);
		}
		return result;
	}

}
