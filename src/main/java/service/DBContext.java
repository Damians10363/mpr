package service;

import java.util.ArrayList;


import database.EntityBase;


public class DBContext<E extends EntityBase> extends RepositoryBase<E>{

	protected MockDb<E> db;
	

	
	public DBContext(MockUnitOfWork<E> unitofwork, MockDb<E> db) {
		super(unitofwork);
		this.db = db;
	}

	public void persistAdd(E obj) {
		db.save(obj);
	}

	public void persistUpdate(E obj) {
		db.update(obj);
	}

	public void persistDelete(E obj) {
		db.delete(obj);
	}

	public E get(int id) {
		
		return db.get(id);
	}

	public ArrayList<E> getAll() {
		return db.getAllItems();
	}
	
	public void Commit(){
		super.unitofwork.Commit();
		db.sort();
	}





}
