package service;

import javax.enterprise.context.Dependent;

import database.EntityBase;

@Dependent
public interface IUnitOfWork<E extends EntityBase> {
	public void RegisterAdd(E key, RepositoryBase<E> value);
	public void RegisterDelete(E key, RepositoryBase<E> value);
	public void RegisterUpdate(E key, RepositoryBase<E> value);
	public void Commit();
	public void Rollback();
}
