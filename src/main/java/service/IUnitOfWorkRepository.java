package service;

import database.EntityBase;


public interface IUnitOfWorkRepository<E extends EntityBase> {
	
	public abstract void persistAdd(E key);
	public abstract void persistUpdate(E key);
	public abstract void persistDelete(E key);
	
}
