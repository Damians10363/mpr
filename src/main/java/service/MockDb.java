package service;

import java.util.ArrayList;

import database.EntityBase;

public class MockDb<E extends EntityBase> {

	private ArrayList<E> allitems;
	private int i = 0;
	public MockDb(ArrayList<E> allitems) {
		super();
		this.allitems = allitems;
	}
	
	public void sort()
	{
		for(int i = 0; i < allitems.size()-1; i++)
		{
			for(int j = 0; j < allitems.size()-i-1; j++)
		{
			if(allitems.get(j).getId() > allitems.get(j+1).getId())
			{
				E tmp = allitems.set(j+1, allitems.get(j));
				allitems.set(j, tmp);
			}
		}
		}
	}
	
	public ArrayList<E> getAllItems()
	{
		return allitems;
	}
	
	public E get(int id)
	{
		return allitems.get(id);
	}
	
	
	public void update(E obj)
	{
		allitems.set(obj.getId(), obj);
	}
	
	public void save(E obj)
	{
		obj.setId(i++);
		allitems.add(obj);
	}
	
	public void delete(E obj)
	{
		allitems.remove(obj);
	}
	
	
	
	
    
    
    
}
