package service;

import java.util.HashMap;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Dependent;
import javax.inject.Named;
import javax.inject.Singleton;

import database.EntityBase;

@Dependent
@Named
public class MockUnitOfWork<E extends EntityBase> implements IUnitOfWork<E>{

	public  Map<E, IUnitOfWorkRepository<E>> added;
	public  Map<E, IUnitOfWorkRepository<E>> updated;
	public  Map<E, IUnitOfWorkRepository<E>> deleted;
	
	public MockUnitOfWork() {
		
		added = new HashMap<E, IUnitOfWorkRepository<E>>();
		updated = new HashMap<E, IUnitOfWorkRepository<E>>();
		deleted = new HashMap<E, IUnitOfWorkRepository<E>>();
	}

	public void RegisterAdd(E key, RepositoryBase<E> value) {
		added.put(key, value);
	}

	public void RegisterDelete(E key, RepositoryBase<E> repositoryBase) {
		deleted.put(key, repositoryBase);
	}

	public void RegisterUpdate(E key, RepositoryBase<E> value) {
		updated.put(key, value);
	}

	public void Commit() {
		for(E key : added.keySet())
		{
			added.get(key).persistAdd(key);
		}
		added.clear();
		for(E key : updated.keySet())
		{
			updated.get(key).persistUpdate(key);
		}
		updated.clear();
		for(E key : deleted.keySet())
		{
			deleted.get(key).persistDelete(key);
		}
		deleted.clear();
	}

	public void Rollback() {
		added.clear();
		deleted.clear();
		updated.clear();
	}

}
