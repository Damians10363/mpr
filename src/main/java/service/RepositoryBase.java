package service;


import java.util.ArrayList;


import database.EntityBase;

public abstract class RepositoryBase<E extends EntityBase> implements RepositoryInterface<E>, IUnitOfWorkRepository<E>{
	
	public MockUnitOfWork<E> unitofwork;
	
	
	public RepositoryBase(MockUnitOfWork<E> unitofwork) {
		super();
		this.unitofwork = unitofwork;
	}

	public abstract void persistAdd(E obj);

	public abstract void persistUpdate(E obj);

	public abstract void persistDelete(E obj);
	
	public void save(E obj) {
		unitofwork.RegisterAdd(obj, this);
	}
	
	public void delete(E obj) {
		unitofwork.RegisterDelete(obj, this);
	}

	public void update(E obj) {
		unitofwork.RegisterUpdate(obj, this);
	}
	public abstract E get(int id);
	
	public abstract ArrayList<E> getAll();		
	

	
	

	

}
