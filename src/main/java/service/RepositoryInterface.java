package service;

import java.util.List;

import database.EntityBase;

public interface RepositoryInterface<E extends EntityBase> {
	
	public void save(E obj);
	public void delete(E obj);
	public E get(int id);
	public List<E> getAll();
	public void update(E obj);
	

}
