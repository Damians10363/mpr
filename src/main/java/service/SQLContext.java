package service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import org.apache.log4j.Logger;

import database.EntityBase;

public abstract class SQLContext<E extends EntityBase> extends RepositoryBase<E>{

	protected PreparedStatement insert;
	protected PreparedStatement delete;
	protected PreparedStatement update;
	protected PreparedStatement getById;
	protected PreparedStatement getAll;
	protected PreparedStatement deleteAll;
	
	protected Logger logger = Logger.getRootLogger();
	protected Statement stmt;
	
	protected Connection connection;
	private String url = "jdbc:mysql://localhost:3306/s10363";
	
	public SQLContext(MockUnitOfWork<E> unitofwork) {
		super(unitofwork);
		try {
			connection = DriverManager.getConnection(url,"root","root");
		} catch (SQLException e) {
			e.printStackTrace();	
		}
	}

	@Override
	public abstract void persistAdd(E obj);
	
	@Override
	public abstract void persistUpdate(E obj);		

	@Override
	public abstract void persistDelete(E obj);

	@Override
	public abstract E get(int id);
	
	@Override
	public abstract ArrayList<E> getAll();

	public void persistDeleteAll(){
		try {
			deleteAll.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}