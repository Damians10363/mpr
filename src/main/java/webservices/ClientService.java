package webservices;

import javax.jws.WebMethod;
import javax.jws.WebService;

import repositories.ClientRepository;
import service.MockUnitOfWork;
import database.Client;
import dto.ClientDto;
import dto.ClientSummaryDto;

@WebService
public class ClientService {

	private ClientRepository cr = new ClientRepository(new MockUnitOfWork<Client>());
	
	@WebMethod
	public ClientDto giveClient(int request) {
		Client c = cr.get(request);
		ClientDto result = new ClientDto();
		result.setName(c.getName());
		result.setAddress(c.getAddress());
		result.setCity(c.getCity());
		result.setPhonenumber(c.getPhonenumber());
		return result;
	}

	@WebMethod
	public ClientSummaryDto saveClient(ClientDto request) {
		Client c = new Client();
		c.setName(request.getName());
		c.setAddress(request.getAddress());
		c.setCity(request.getCity());
		c.setPhonenumber(request.getPhonenumber());
		cr.save(c);
		cr.unitofwork.Commit();
		ClientSummaryDto result = new ClientSummaryDto();
		result.setName(c.getName());
		result.setAddress(c.getAddress());
		result.setCity(c.getCity());
		result.setPhonenumber(c.getPhonenumber());
		return result;

	}
	
	
}