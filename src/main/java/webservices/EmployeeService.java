package webservices;

import javax.jws.WebMethod;
import javax.jws.WebService;

import database.Employee;
import dto.EmployeeDto;
import dto.EmployeeSummaryDto;
import repositories.EmployeeRepository;
import service.MockUnitOfWork;

@WebService
public class EmployeeService {

	private EmployeeRepository cr = new EmployeeRepository(new MockUnitOfWork<Employee>());

	@WebMethod
	public EmployeeDto getEmployee(int request) {
		Employee e = cr.get(request);
		EmployeeDto result = new EmployeeDto();
		result.setName(e.getName());
		result.setSurname(e.getSurname());
		result.setDob(e.getDob());
		result.setSalary(e.getSalary());
		result.setPhonenumber(e.getPhonenumber());
		return result;
	}

	@WebMethod
	public EmployeeSummaryDto saveEmployee(EmployeeDto request) {

		Employee e = new Employee();
		e.setName(request.getName());
		e.setSurname(request.getSurname());
		e.setDob(request.getDob());
		e.setSalary(request.getSalary());
		e.setPhonenumber(request.getPhonenumber());
		cr.save(e);
		cr.unitofwork.Commit();
		EmployeeSummaryDto result = new EmployeeSummaryDto();
		result.setName(e.getName());
		result.setSurname(e.getSurname());
		result.setDob(e.getDob());
		result.setSalary(e.getSalary());
		result.setPhonenumber(e.getPhonenumber());
		return result;
	}

}