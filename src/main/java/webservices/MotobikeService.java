package webservices;

import javax.jws.WebMethod;
import javax.jws.WebService;

import database.Motobike;
import dto.MotobikeDto;
import dto.MotobikeSummaryDto;
import repositories.MotobikeRepository;
import service.MockUnitOfWork;

@WebService
public class MotobikeService {

	private MotobikeRepository cr = new MotobikeRepository(new MockUnitOfWork<Motobike>());

	@WebMethod
	public MotobikeDto giveMotobike(int request) {
		Motobike c = cr.get(request);
		MotobikeDto result = new MotobikeDto();
		result.setBrand(c.getBrand());
		result.setType(c.getType());
		result.setEnginecapacity(c.getEnginecapacity());
		result.setLicenseplate(c.getLicenseplate());
		result.setTimesunderrepair(c.getTimesunderrepair());
		return result;
	}

	@WebMethod
	public MotobikeSummaryDto saveMotobike(MotobikeDto request) {
		Motobike motobike = new Motobike();
		motobike.setBrand(request.getBrand());
		motobike.setType(request.getType());
		motobike.setEnginecapacity(request.getEnginecapacity());
		motobike.setLicenseplate(request.getLicenseplate());
		motobike.setTimesunderrepair(request.getTimesunderrepair());
		cr.save(motobike);
		cr.unitofwork.Commit();
		MotobikeSummaryDto result = new MotobikeSummaryDto();
		result.setBrand(motobike.getBrand());
		result.setType(motobike.getType());
		result.setEnginecapacity(motobike.getEnginecapacity());
		result.setLicenseplate(motobike.getLicenseplate());
		result.setTimesunderrepair(motobike.getTimesunderrepair());
		return result;

	}

}