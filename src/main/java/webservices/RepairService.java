package webservices;

import database.Repair;
import dto.RepairDto;
import repositories.RepairRepository;
import service.MockUnitOfWork;
import dto.RepairSummaryDto;
import javax.jws.WebMethod;
import javax.jws.WebService;

public class RepairService {

	private RepairRepository cr = new RepairRepository(new MockUnitOfWork<Repair>());
	
	@WebMethod
	public RepairDto giveRepair(int request) {
		Repair r = cr.get(request);
		RepairDto result = new RepairDto();
		result.setId(r.getId());
		result.setClientId(r.getClientId());
		result.setMotobikeId(r.getMotobikeId());
		result.setWorkplaceId(r.getWorkplaceId());
		return result;
	}

	@WebMethod
	public RepairSummaryDto saveRepair(int ClientId, int MotobikeId, int WorkplaceId) {
		Repair r = new Repair();
		r.setClientId(ClientId);
		r.setMotobikeId(MotobikeId);
		r.setWorkplaceId(WorkplaceId);
		cr.save(r);
		cr.unitofwork.Commit();
		RepairDto result = new RepairDto();
		result.setClientId(r.getClientId());
		result.setMotobikeId(r.getMotobikeId());
		result.setWorkplaceId(r.getWorkplaceId());
		return result;

	}
	
}
