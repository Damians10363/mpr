package mockrepositoriestests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import mockrepositories.ClientRepository;

import org.junit.Before;
import org.junit.Test;

import database.Client;
import service.MockDb;
import service.MockUnitOfWork;

public class ClientRepositoryTest {
	
	private final String NAME = "MotorCompany";
	private final String ADDRESS = "AnvilStrasse";
	private final String CITY = "Bremen";
	private final int PHONENUMBER = 6095321;
	private final String NAME_2 = "MotorradWerkstatt";
	
	private final Client CLIENT_1 = new Client(NAME, ADDRESS, CITY, PHONENUMBER, null);
	
	private ClientRepository cr;
	private MockDb<Client> db;
	private MockUnitOfWork<Client> uof;
	
	private Client client;
	private Client client2;
	
	@Before
	public void setUp() throws Exception {
		db = new MockDb<Client>(new ArrayList<Client>());
		uof = new MockUnitOfWork<Client>();
	}

	@Test
	public void testPersistAdd() {
		cr = new ClientRepository(uof, db);
		cr.persistAdd(CLIENT_1);
		assertFalse("prersistAdd fail", cr.getAll().isEmpty());
		client = cr.get(0);
		assertEquals("different Client", CLIENT_1, client);
	}

	@Test
	public void testPersistUpdate() {
		db.save(CLIENT_1);
		cr = new ClientRepository(uof, db);
		Client client2 = new Client (NAME, ADDRESS, CITY, PHONENUMBER, null);
		client2.setId(0);
		assertEquals("different name", CLIENT_1.getName());
		assertEquals("different address", CLIENT_1.getAddress(), client2.getAddress());
		assertEquals("different city", CLIENT_1.getCity(), client2.getCity());
		client2.setName(NAME_2);
		cr.persistUpdate(client2);
		assertEquals("different name", NAME_2, cr.get(0).getName());
	}

	@Test
	public void testPersistDelete() {
		db.save(CLIENT_1);
		assertEquals("different Client", CLIENT_1, db.get(0));
		cr = new ClientRepository(uof, db);
		assertFalse("empty record", cr.getAll().isEmpty());
		cr.persistDelete(CLIENT_1);
		assertTrue("not empty record", cr.getAll().isEmpty());
	}

	@Test
	public void testGet() {
		db.save(CLIENT_1);
		db.save(new Client());
		cr = new ClientRepository(uof, db);
		client = cr.get(0);
		client2 = cr.get(1);
		assertNotNull("problem with getting client", client);
		assertTrue("problem with getting name", client.getName().equals(NAME));
		assertTrue("problem with getting address", client.getAddress().equals(ADDRESS));
		assertTrue("problem with getting city", client.getCity().equals(CITY));
		assertNotSame("objects are in the same memory address", client, client2);
		
	}

	@Test
	public void testGetAll() {
		db.save(CLIENT_1);
		db.save(new Client());
		db.save(new Client());
		db.save(new Client());
		db.save(new Client());
		cr = new ClientRepository(uof, db);
		ArrayList<Client> result;
		result = cr.getAll();
		assertEquals("different client", CLIENT_1, result.get(0));
		assertFalse("record is empty", result.isEmpty());
		
	}

	@Test
	public void testSave() {
		cr = new ClientRepository(uof, db);
		cr.save(CLIENT_1);
		cr.Commit();
		client = cr.get(0);
		assertEquals("different name", NAME, client.getName());
		assertEquals("different address", ADDRESS, client.getAddress());
		assertEquals("different city", CITY, client.getCity());
	}

	@Test
	public void testDelete() {
		db.save(CLIENT_1);
		cr = new ClientRepository(uof, db);
		assertTrue("different client", CLIENT_1.equals(cr.get(0)));
		cr.delete(CLIENT_1);
		cr.Commit();
		assertTrue("not deleted", cr.getAll().isEmpty());
	}

	@Test
	public void testUpdate() {
		cr = new ClientRepository(uof, db);
		cr.save(CLIENT_1);
		client = new Client(NAME, ADDRESS, CITY, PHONENUMBER, null);
		client.setId(0);
		assertEquals("different ID", CLIENT_1.getId(), client.getId());
		assertEquals("different name", CLIENT_1.getName(), client.getName());
		assertEquals("different city", CLIENT_1.getCity(), client.getCity());
		assertEquals("different phonenumber", CLIENT_1.getPhonenumber(), client.getPhonenumber());
		client.setName(NAME_2);
		cr.update(client);
		cr.Commit();
		assertEquals("different name", NAME_2, cr.get(0).getName());
	}

}
