package mockrepositoriestests;

import static org.junit.Assert.*;
import mockrepositories.EmployeeRepository;

import org.junit.Before;
import org.junit.Test;

import service.MockDb;
import service.MockUnitOfWork;
import database.Employee;
import database.Repair;

import java.util.ArrayList;
import java.util.List;

public class EmployeeRepositoryTest {
	
	private final String NAME = "Adam";
	private final String SURNAME = "Nowak";
	private final int DOB = 1990-02-14;
	private final int SALARY = 3000;
	private final int PHONENUMBER = 6538430;
	private final ArrayList<Repair> REPAIRS_1 = new ArrayList<Repair>();
	//private final ArrayList<Repair> REPAIRS_2 = new ArrayList<Repair>();
	
	private final Employee EMPLOYEE_1 = new Employee(NAME, SURNAME, DOB, SALARY, PHONENUMBER, REPAIRS_1);
	private Employee employee;
	private Employee employee2;
	
	private EmployeeRepository cr;
	private MockDb<Employee> db;
	private MockUnitOfWork<Employee> uof;
	
	@Before
	public void setUp() throws Exception{
	db = new MockDb<Employee>(new ArrayList<Employee>());
	uof = new MockUnitOfWork<Employee>();
	}
	
	@Test
	public void testPersistAdd() {
	cr = new EmployeeRepository(uof, db);
	cr.persistAdd(EMPLOYEE_1);
	assertFalse("PersistAdd fail", cr.getAll().isEmpty());
	employee = cr.get(0);
	assertEquals("different Employee", EMPLOYEE_1, employee);
	}

	@Test
	public void testPersistUpdate() {
		db.save(EMPLOYEE_1);
		cr = new EmployeeRepository(uof,db);
		employee = new Employee(NAME, SURNAME, DOB, SALARY, PHONENUMBER, REPAIRS_1);
		employee.setId(0);
		assertEquals("different name", EMPLOYEE_1.getName(), employee.getName());
		assertEquals("different surname", EMPLOYEE_1.getName(), employee.getName());
		assertEquals("different date-of-birth", EMPLOYEE_1.getDob(), employee.getDob());
		assertEquals("different salary", EMPLOYEE_1.getSalary(), employee.getSalary());
		assertEquals("different phonenumber", EMPLOYEE_1.getPhonenumber(), employee.getPhonenumber());
		//assertEquals("different number of repairs", EMPLOYEE_1.getRepairs(), employee.getRepairs());
		//employee.setRepairs(REPAIRS_2);
		cr.persistUpdate(employee);
		//assertEquals("different number of repairs", REPAIRS_2, cr.get(0).getRepairs());
		
	}

	@Test
	public void testPersistDelete() {
		db.save(EMPLOYEE_1);
		assertEquals("not saved", EMPLOYEE_1, db.get(0));
		cr = new EmployeeRepository(uof, db);
		assertFalse("base is empty", cr.getAll().isEmpty());
		cr.persistDelete(EMPLOYEE_1);
		assertTrue("base is NOT empty", cr.getAll().isEmpty());
	}

	@Test
	public void testGet() {
		db.save(EMPLOYEE_1);
		db.save(new Employee());
		cr = new EmployeeRepository(uof,db);
		employee = cr.get(0);
		employee2 = cr.get(1);
		assertNotNull("getting failed", employee);
		assertTrue("different name", employee.getName().equals(NAME));
		assertTrue("different surname", employee.getSurname().equals(SURNAME));
		assertEquals("differenet date-of-birth", DOB, employee.getDob());
		assertEquals("differenet salary", SALARY, employee.getSalary());
		assertEquals("differenet phonenumber", PHONENUMBER, employee.getPhonenumber());
		//assertEquals("different number of repairs", REPAIRS_1, employee.getRepairs());
		assertNotSame("objects are matching the same memory space", employee, employee2);
	}

	@Test
	public void testGetAll() {
		db.save(EMPLOYEE_1);
		db.save(new Employee());
		db.save(new Employee());
		cr = new EmployeeRepository(uof, db);
		List<Employee> result;
		result = cr.getAll();
		assertEquals("different employee", EMPLOYEE_1, result.get(0));
		assertFalse("empty", result.isEmpty());
	}

	@Test
	public void testSave() {
		cr = new EmployeeRepository(uof, db);
		cr.save(EMPLOYEE_1);
		cr.Commit();
		employee = cr.get(0);
		assertEquals("different name", NAME, employee.getName());
		assertEquals("different surname", SURNAME, employee.getSurname());
		assertEquals("differenet date-of-birth", DOB, employee.getDob());
		assertEquals("differenet salary", SALARY, employee.getSalary());
		assertEquals("differenet phonenumber", PHONENUMBER, employee.getPhonenumber());
		//assertEquals("different number of repairs", REPAIRS_1, employee.getRepairs());
	}

	@Test
	public void testDelete() {
		cr = new EmployeeRepository(uof, db);
		cr.save(EMPLOYEE_1);
		cr.Commit();
		assertTrue("different phonenumber", EMPLOYEE_1.equals(cr.get(0)));
		cr.delete(EMPLOYEE_1);
		cr.Commit();
		assertTrue("not deleted", cr.getAll().isEmpty());
	}

	@Test
	public void testUpdate() {
		cr = new EmployeeRepository(uof, db);
		cr.save(EMPLOYEE_1);
		cr.Commit();
		employee = new Employee(NAME, SURNAME, DOB, SALARY, PHONENUMBER, REPAIRS_1);
		employee.setId(0);
		assertEquals("different name", NAME, employee.getName());
		assertEquals("different surname", SURNAME, employee.getSurname());
		assertEquals("differenet date-of-birth", DOB, employee.getDob());
		assertEquals("differenet salary", SALARY, employee.getSalary());
		assertEquals("differenet phonenumber", PHONENUMBER, employee.getPhonenumber());
		//assertEquals("different number of repairs", REPAIRS_1, employee.getRepairs());
		//employee.setRepairs(REPAIRS_2);
		cr.update(employee);
		cr.Commit();
		//assertEquals("different number of repairs", REPAIRS_2, cr.get(0).getRepairs());
	}

}
