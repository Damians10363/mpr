package mockrepositoriestests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import mockrepositories.MotobikeRepository;

import org.junit.Before;
import org.junit.Test;

import service.MockDb;
import service.MockUnitOfWork;
import database.Motobike;

public class MotobikeRepositoryTest {
	
	private final String LICENSEPLATE = "GKA6080";
	private final String LICENSEPLATE_2 = "GDA8032";
	private final String BRAND = "Honda";
	private final String TYPE = "CBR";
	private final int ENGINECAPACITY = 600;
	private final int TIMESUNDERREPAIR = 1;
	
	private final Motobike M_1 = new Motobike(LICENSEPLATE, BRAND, TYPE, ENGINECAPACITY, TIMESUNDERREPAIR);
	private final Motobike M_2 = new Motobike("GWE5410", "Yamaha","YBR", 250, 2);
	private final Motobike M_3 = new Motobike("GPU4170", "Suzuki","GSXR", 750, 0);
	
	private Motobike motobike;
	private Motobike motobike2;
	
	private MotobikeRepository cr;
	private MockUnitOfWork<Motobike> uof;
	private MockDb<Motobike> db;

	@Before
	public void setUp() throws Exception{
			db = new MockDb<Motobike>(new ArrayList<Motobike>());
			uof = new MockUnitOfWork<Motobike>();
	}
	
	@Test
	public void testPersistAdd() {
		cr = new MotobikeRepository(uof, db);
		cr.persistAdd(M_2);
		assertFalse("persist add fail", cr.getAll().isEmpty());
		motobike = cr.get(0);
		assertEquals("different motobike", M_2, motobike);
	}

	@Test
	public void testPersistUpdate() {
		db.save(M_1);
		cr = new MotobikeRepository(uof, db);
		motobike = new Motobike(LICENSEPLATE, BRAND, TYPE, ENGINECAPACITY, TIMESUNDERREPAIR);
		motobike.setId(0);
		assertEquals("different license-plate", M_1.getLicenseplate(), motobike.getLicenseplate());
		assertEquals("different brand", M_1.getBrand(), motobike.getBrand());
		assertEquals("different type", M_1.getType(), motobike.getType());
		assertEquals("different engine-capacity", M_1.getEnginecapacity(), motobike.getEnginecapacity());
		assertEquals("different times-under-repair", M_1.getTimesunderrepair(), motobike.getTimesunderrepair());
		motobike.setLicenseplate(LICENSEPLATE_2);
		cr.persistUpdate(motobike);
		assertEquals("different license-plate", LICENSEPLATE_2, cr.get(0).getLicenseplate());
	}

	@Test
	public void testPersistDelete() {
		db.save(M_1);
		assertEquals("not saved", M_1, db.get(0));
		cr = new MotobikeRepository(uof, db);
		assertFalse("base is empty", cr.getAll().isEmpty());
		cr.persistDelete(M_1);
		assertTrue("base isn't empty", cr.getAll().isEmpty());
	}

	@Test
	public void testGet() {
		db.save(M_1);
		db.save(M_2);
		cr = new MotobikeRepository(uof, db);
		motobike = cr.get(0);
		motobike2 = cr.get(1);
		assertNotNull("getting motobike form base fail", motobike);
		assertTrue("problem with getting license-plate", motobike.getLicenseplate().equals(LICENSEPLATE));
		assertTrue("problem with getting brand", motobike.getBrand().equals(BRAND));
		assertTrue("problem with getting type", motobike.getType().equals(TYPE));
		assertEquals("different engine-capacity", motobike.getEnginecapacity(), ENGINECAPACITY);
		assertEquals("different times-under-repair", motobike.getTimesunderrepair(), TIMESUNDERREPAIR);
		assertNotSame("objects are matching the same memory space", motobike, motobike2);
		
	}

	@Test
	public void testGetAll() {
		db.save(M_1);
		db.save(M_2);
		db.save(M_3);
		cr = new MotobikeRepository(uof, db);
		ArrayList<Motobike> result;
		result = cr.getAll();
		assertFalse("base is empty", result.isEmpty());
		assertEquals("different motobikes", M_1, result.get(0));
		assertEquals("different motobikes", M_2, result.get(1));
		assertEquals("different motobikes", M_2, result.get(2));
	}

	@Test
	public void testSave() {
		cr = new MotobikeRepository(uof, db);
		cr.save(M_1);
		cr.Commit();
		Motobike motobike = cr.get(0);
		assertEquals("different license-plate", LICENSEPLATE, motobike.getLicenseplate());
		assertEquals("different brand", BRAND, motobike.getBrand());
		assertEquals("different type", TYPE, motobike.getType());
		assertEquals("different engine-capacity", ENGINECAPACITY, motobike.getEnginecapacity());
		assertEquals("different times-under-repair", TIMESUNDERREPAIR, motobike.getTimesunderrepair());
	}

	@Test
	public void testDelete() {
		db.save(M_2);
		cr = new MotobikeRepository(uof, db);
		assertTrue("different motobike", M_2.equals(cr.get(0)));
		cr.delete(M_2);
		cr.Commit();
		assertTrue("not deleted", cr.getAll().isEmpty());
	}

	@Test
	public void testUpdate() {
		cr = new MotobikeRepository(uof, db);
		cr.save(M_1);
		motobike = new Motobike(LICENSEPLATE, BRAND, TYPE, ENGINECAPACITY, TIMESUNDERREPAIR);
		motobike.setId(0);
		assertEquals("different ID", M_1.getId(), motobike.getId());
		assertEquals("different licenseplate", M_1.getLicenseplate(), motobike.getLicenseplate());
		assertEquals("different brand", M_1.getBrand(), motobike.getBrand());
		assertEquals("different type", M_1.getType(), motobike.getType());
		assertEquals("different engine-capacty", M_1.getEnginecapacity(), motobike.getEnginecapacity());
		motobike.setLicenseplate(LICENSEPLATE_2);
		cr.update(motobike);
		cr.Commit();
		assertEquals("different license-plate", LICENSEPLATE_2, cr.get(0).getLicenseplate());
	}

}
