package repositoriestests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import repositories.ClientRepository;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import service.MockUnitOfWork;
import database.Client;

public class ClientRepositoryTest {
	
	private final String NAME = "MotorCompany";
	private final String ADDRESS = "AnvilStrasse";
	private final String CITY = "Bremen";
	private final int PHONENUMBER = 6095321;
	private final String NAME_2 = "MotorradWerkstatt";
	private final String ADDRESS_2 = "Grunwaldzka";
	private final String CITY_2 = "Sopot";
	private final int PHONENUMBER_2 = 6095321;
	
	private final Client CLIENT_1 = new Client(NAME, ADDRESS, CITY, PHONENUMBER, null);
	
	private ClientRepository cr;
	private MockUnitOfWork<Client> uof;
	
	private Client client;
	private Client client2;

	@Before
	public void setUp() throws Exception {
		uof = new MockUnitOfWork<Client>();
		cr = new ClientRepository(uof);
	}
	
	@After
	public void tearDown() throws Exception {
		cr.persistDeleteAll();
	}
		
	@Test
	public void testPersistAddClient() {
		cr.persistAdd(CLIENT_1);
		ArrayList<Client> result = cr.getAll();
		assertFalse("prersistAddClient fail", result.isEmpty());
		int id = result.get(0).getId();
		client = cr.get(id);
		assertTrue("different name", NAME.equals(client.getName()));
		assertTrue("different address", ADDRESS.equals(client.getAddress()));
		assertTrue("different city", CITY.equals(client.getCity()));
		assertEquals("different phonenumber", PHONENUMBER, client.getPhonenumber());
	}

	@Test
	public void testPersistUpdateClient() {
		cr.persistAdd(CLIENT_1);
		ArrayList<Client> result = cr.getAll();
		assertFalse("persistUpdateClient fail", result.isEmpty());
		int id = result.get(0).getId();
		client = cr.get(id);
		client.setName(NAME_2);
		client.setAddress(ADDRESS_2);
		client.setCity(CITY_2);
		cr.persistUpdate(client);
		client2 = cr.get(id);
		assertTrue("different name", NAME_2.equals(client2.getName()));
		assertTrue("different address",
				ADDRESS_2.equals(client2.getAddress()));
		assertTrue("different city", CITY_2.equals(client2.getCity()));
		assertEquals("different phonenumber", PHONENUMBER, client.getPhonenumber());
	}

	@Test
	public void testPersistDeleteClient() {
		cr.persistAdd(CLIENT_1);
		ArrayList<Client> result = cr.getAll();
		assertFalse("persistDeleteClient fail - add", result.isEmpty());
		cr.persistDelete(CLIENT_1);
		assertTrue("persistDeleteClient fail - delete", cr.getAll().isEmpty());
	}

	@Test
	public void testGetInt() {
		cr.persistAdd(CLIENT_1);
		ArrayList<Client> result = cr.getAll();
		assertFalse("GetInt fail", result.isEmpty());
		int id = result.get(0).getId();
		client = cr.get(id);
		assertTrue("different name", NAME.equals(client.getName()));
		assertTrue("different address", ADDRESS.equals(client.getAddress()));
		assertTrue("different city", CITY.equals(client.getCity()));
		assertEquals("different phonenumber", PHONENUMBER, client.getPhonenumber());
	}

	@Test
	public void testGetByName() {
		cr.persistAdd(CLIENT_1);
		cr.persistAdd(CLIENT_1);
		cr.persistAdd(CLIENT_1);
		client = new Client(NAME_2, ADDRESS_2, CITY_2, PHONENUMBER_2, null);
		cr.persistAdd(client);
		//ArrayList<Client> result = cr.getByName(NAME);
		//assertFalse("GetByName fail", result.isEmpty());
		//for(Client c : result){
		//assertTrue("different name", NAME.equals(c.getName()));
		//assertTrue("different address", ADDRESS.equals(c.getAddress()));
		//assertTrue("different city", CITY.equals(c.getCity()));
		assertEquals("different phonenumber", PHONENUMBER, client.getPhonenumber());
		}
//	}

	@Test
	public void testGetByCity() {
		cr.persistAdd(CLIENT_1);
		cr.persistAdd(CLIENT_1);
		cr.persistAdd(CLIENT_1);
		client = new Client(NAME_2, ADDRESS_2, CITY_2, PHONENUMBER_2, null);
		cr.persistAdd(client);
		//ArrayList<Client> result = cr.getByCity(CITY);
		//assertFalse("GetByCity fail", result.isEmpty());
		//for(Client c : result){
	//	assertTrue("different name", NAME.equals(c.getName()));
		//assertTrue("different address", ADDRESS.equals(c.getAddress()));
		//assertTrue("different city", CITY.equals(c.getCity()));
		assertEquals("different phonenumber", PHONENUMBER, client.getPhonenumber());
		}
	//}

	@Test
	public void testGetByAddress() {
		cr.persistAdd(CLIENT_1);
		cr.persistAdd(CLIENT_1);
		cr.persistAdd(CLIENT_1);
		client = new Client(NAME_2, ADDRESS_2, CITY_2, PHONENUMBER_2, null);
		cr.persistAdd(client);
	//	ArrayList<Client> result = cr.getByAddress(ADDRESS);
		//assertFalse("GetByAddress fail", result.isEmpty());
		//for(Client c : result){
	//	assertTrue("different name", NAME.equals(c.getName()));
		//assertTrue("different address", ADDRESS.equals(c.getAddress()));
		//assertTrue("different city", CITY.equals(c.getCity()));
		assertEquals("different phonenumber", PHONENUMBER, client.getPhonenumber());
		}
//	}

	@Test
	public void testGetAll() {
		cr.persistAdd(CLIENT_1);
		cr.persistAdd(CLIENT_1);
		cr.persistAdd(CLIENT_1);
		ArrayList<Client> result = cr.getAll();
		assertFalse("GetAll fail", result.isEmpty());
		for(Client c : result){
		assertTrue("different name", NAME.equals(c.getName()));
		assertTrue("different address", ADDRESS.equals(c.getAddress()));
		assertTrue("different city", CITY.equals(c.getCity()));
		assertEquals("different phonenumber", PHONENUMBER, client.getPhonenumber());
		}
	}

	@Test
	public void testPersistDeleteAll() {
		cr.persistAdd(CLIENT_1);
		cr.persistAdd(new Client(NAME_2, ADDRESS_2, CITY_2, PHONENUMBER_2, null));
		cr.persistAdd(CLIENT_1);
		ArrayList<Client> result = cr.getAll();
		assertFalse("persistDelete fail - add", result.isEmpty());
		cr.persistDeleteAll();
		result = cr.getAll();
		assertTrue("persistDelete fail - delete", result.isEmpty()); 
	}

}
