package repositoriestests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import repositories.EmployeeRepository;
//import service.MockDb;
import service.MockUnitOfWork;
import database.Employee;
import database.Repair;

public class EmployeeRepositoryTest {
	
	private final String NAME = "Adam";
	private final String NAME_2 = "Maciej";
	private final String SURNAME = "Nowak";
	private final String SURNAME_2 = "Krawczyk";
	private final int DOB = 1990-02-14;
	private final int DOB_2 = 1994-03-03;
	private final int SALARY = 3000;
//	private final int SALARY_2 = 4500;
	private final int PHONENUMBER = 6538430;
	//private final int PHONENUMBER_2 = 5636041;
	private final ArrayList<Repair> REPAIRS_1 = new ArrayList<Repair>();
	//private final ArrayList<Repair> REPAIRS_2 = new ArrayList<Repair>();

	private Employee EMPLOYEE;
	private Employee employee;
	private Employee employee2;

	private EmployeeRepository cr;
	private MockUnitOfWork<Employee> uof;
	
	
	@Before
	public void setUp() throws Exception{
	uof = new MockUnitOfWork<Employee>();
	cr = new EmployeeRepository(uof);
	EMPLOYEE = new Employee(NAME, SURNAME, DOB, SALARY, PHONENUMBER, REPAIRS_1);
	}
	
	@After
	public void tearDown() throws Exception {
		cr.persistDeleteAll();
	}
	
	@Test
	public void testPersistAddEmployee() {
		cr.persistAdd(EMPLOYEE);
		ArrayList<Employee> result = new ArrayList<Employee>();
		result = cr.getAll();
		assertFalse("PersistAddEmployee fail", result.isEmpty());
		//employee = cr.getByDob(DOB);
		assertTrue("different name", NAME.equals(employee.getName()));
		assertTrue("different surname", SURNAME.equals(employee.getSurname()));
		assertEquals("different id", DOB, employee.getDob());
	//	assertEquals("different list of orders", REPAIRS_1,
		//		employee.getRepairs());
	}

	@Test
	public void testPersistUpdateEmployee() {
		cr.persistAdd(EMPLOYEE);
		ArrayList<Employee> result = new ArrayList<Employee>();
		result = cr.getAll();
		assertFalse("not added", result.isEmpty());
		//employee = cr.getByDob(DOB);
		employee.setName(NAME_2);
		employee.setSurname(SURNAME_2);
		employee.setDob(DOB_2);
		//employee.setRepairs(REPAIRS_2);
		cr.persistUpdate(employee);
		//employee2 = cr.getByDob(DOB_2);
		assertTrue("different name", NAME_2.equals(employee2.getName()));
		assertTrue("different surname",
				SURNAME_2.equals(employee2.getSurname()));
		assertEquals("different date-of-birth", DOB_2, employee2.getDob());
	//	assertEquals("different list of repairs", REPAIRS_2,
			//employee2.getRepairs());
	}

	@Test
	public void testPersistDeleteEmployee() {
		cr.persistAdd(EMPLOYEE);
		ArrayList<Employee> result = new ArrayList<Employee>();
		result = cr.getAll();
		assertFalse("PersistDeleteEmployee fail - add", result.isEmpty());
		cr.persistDelete(EMPLOYEE);
		result = cr.getAll();
		assertTrue("PersistDeleteEmployee - delete", result.isEmpty());
		//employee = cr.getByDob(DOB);
	}

	@Test
	public void testGetInt() {
		cr.persistAdd(EMPLOYEE);
		ArrayList<Employee> result = new ArrayList<Employee>();
		result = cr.getAll();
		assertFalse("GetInt fail - add", result.isEmpty());
		//int dob = cr.getByDob(DOB).getDob();
	//	employee = cr.get(dob);
		assertTrue("different name", NAME.equals(employee.getName()));
		assertTrue("different surname", SURNAME.equals(employee.getSurname()));
		assertEquals("different date-if-birth", DOB, employee.getDob());
		//assertEquals("different repairs", REPAIRS_1, employee.getRepairs());
	}

	@Test
	public void testGetByName() {
		cr.persistAdd(EMPLOYEE);
		ArrayList<Employee> result = new ArrayList<Employee>();
		result = cr.getAll();
		assertFalse("GetByName fail - add", result.isEmpty());
		//employee = cr.getByName(NAME);
		assertTrue("different name", NAME.equals(employee.getName()));
		assertTrue("different surname", SURNAME.equals(employee.getSurname()));
		assertEquals("different date-of-birth", DOB, employee.getDob());
		//assertEquals("different repairs", REPAIRS_1, employee.getRepairs());
	}

	@Test
	public void testGetBySurname() {
		cr.persistAdd(EMPLOYEE);
		ArrayList<Employee> result = new ArrayList<Employee>();
		result = cr.getAll();
		assertFalse("GetBySurname - add", result.isEmpty());
		//employee = cr.getBySurname(SURNAME);
		assertTrue("different name", NAME.equals(employee.getName()));
		assertTrue("different surname", SURNAME.equals(employee.getSurname()));
		assertEquals("different date-of-birth", DOB, employee.getDob());
		//assertEquals("different repairs", REPAIRS_1, employee.getRepairs());
	}

	@Test
	public void testGetByDob() {
		cr.persistAdd(EMPLOYEE);
		ArrayList<Employee> result = new ArrayList<Employee>();
		result = cr.getAll();
		assertFalse("GetByDob - add", result.isEmpty());
		//employee = cr.getByDob(DOB);
		assertTrue("different name", NAME.equals(employee.getName()));
		assertTrue("different surname", SURNAME.equals(employee.getSurname()));
		assertEquals("different date-of-birth", DOB, employee.getDob());
		//assertEquals("different repairs", REPAIRS_1, employee.getRepairs());
	}

	@Test
	public void testGetAll() {
		cr.persistAdd(EMPLOYEE);
		cr.persistAdd(EMPLOYEE);
		cr.persistAdd(EMPLOYEE);
		ArrayList<Employee> result = new ArrayList<Employee>();
		result = cr.getAll();
		assertFalse("GetAll fail - add", result.isEmpty());
		for(Employee c : result){
			assertTrue("different name", NAME.equals(c.getName()));
			assertTrue("different surname", SURNAME.equals(c.getSurname()));
			assertEquals("different date-of-birth", DOB, c.getDob());
		//	assertEquals("different repairs", REPAIRS_1, c.getRepairs());
		}
	}

	@Test
	public void testPersistDeleteAll() {
		cr.persistAdd(EMPLOYEE);
		cr.persistAdd(EMPLOYEE);
		cr.persistAdd(EMPLOYEE);
		ArrayList<Employee> result = new ArrayList<Employee>();
		result = cr.getAll();
		assertFalse("PersistDeleteAll - add", result.isEmpty());
		cr.persistDeleteAll();
		result = cr.getAll();
		assertTrue("PersistDeleteAll - delete", result.isEmpty());
	}

}
