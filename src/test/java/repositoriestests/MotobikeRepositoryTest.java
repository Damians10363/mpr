package repositoriestests;

import static org.junit.Assert.*;
import repositories.MotobikeRepository;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import service.MockDb;
import service.MockUnitOfWork;
import database.Motobike;

public class MotobikeRepositoryTest {

	private final String LICENSEPLATE = "GKA6080";
	private final String LICENSEPLATE_2 = "GDA8032";
	private final String BRAND = "Honda";
	private final String TYPE = "CBR";
	private final int ENGINECAPACITY = 600;
	private final int TIMESUNDERREPAIR = 1;
	
	private Motobike motobike;
	private Motobike motobike2;
	
	private MotobikeRepository cr;
	private MockUnitOfWork<Motobike> uof;
	
	
	@Before
	public void setUp() throws Exception {
		uof = new MockUnitOfWork<Motobike>();
		cr = new MotobikeRepository(uof);
	}

	@After
	public void tearDown() throws Exception {
		cr.persistDeleteAll();
	}
	@Test
	public void testPersistAddMotobike() {
		fail("Not yet implemented");
	}

	@Test
	public void testPersistUpdateMotobike() {
		fail("Not yet implemented");
	}

	@Test
	public void testPersistDeleteMotobike() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetInt() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetByLicensePlate() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetByBrand() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetByType() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetAll() {
		fail("Not yet implemented");
	}

	@Test
	public void testPersistDeleteAll() {
		fail("Not yet implemented");
	}

}
